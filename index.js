/*1*/


/*2*/


/*3*/
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json))
/*4*/

async function fetchData(){
				
				let myArr = []
				let result = await fetch('https://jsonplaceholder.typicode.com/todos')
				

				
				console.log(myArr)

				let json = await result.json()
				myArr.map((json) => json.title())
				console.log(myArr)
			}

			 fetchData();


/*5*/
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json))
/*6*/
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log( json))
/*7*/
fetch('https://jsonplaceholder.typicode.com/todos',{
	method: 'POST',
	headers:{
		'Content-Type' : 'application/json'
	},
	body : JSON.stringify({
		
			userId: 101,
			id: 5,
			title: "hello world",
			completed: false

		})
})
.then((response) => response.json())
.then((json) => console.log(json))
/*8*/
fetch('https://jsonplaceholder.typicode.com/todos/14',{
	method: 'PUT',
	headers:{
		'Content-Type' : 'application/json'
	},
	body : JSON.stringify({
		userId: 101,
		id: 5,
		title: "hello world",
		completed: false

	})
})
.then((response) => response.json())
.then((json) => console.log(json))
/*9*/
fetch('https://jsonplaceholder.typicode.com/todos/12',{
	method: 'PATCH',
	headers:{
		'Content-Type' : 'application/json'
	},
	body : JSON.stringify({
		userId: 25,
		id: 5,
		title: "wala talaga",
		completed: true

	})
})
.then((response) => response.json())
.then((json) => console.log(json))
/*10*/

/*11*/

fetch('https://jsonplaceholder.typicode.com/todos/12',{
	method: 'PATCH',
	headers:{
		'Content-Type' : 'application/json'
	},
	body : JSON.stringify({
		completed: "complete",
		date: "oct 26 2021"

	})
})
.then((response) => response.json())
.then((json) => console.log(json))
/*12*/

/*13*/

/*14*/

/*15*/